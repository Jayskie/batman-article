#import "template.typ": *

// Take a look at the file `template.typ` in the file panel
// to customize this template and discover how it works.
#show: project.with(
  title: "B.A.T.M.A.N: Wie ich lernte, mich nicht sorgen und den Rückschritt zu lieben",
  // date: datetime(year: 2023, month: 6, day: 28),
  authors: (
    "Josha Bartsch",
  ),
)

#let def(body) = figure(body, supplement: [], kind: "definition")

// We generated the example code below so you can see how
// your document will look. Go ahead and replace it with
// your own content!

= Motivation
In unserer Nachbarschaft haben wir Freifunk. Was heisst das?

Es heisst das viele Router in den umliegenden Gebäuden sich per WLAN miteinander verbinden und sich austauschen, die Internetzugänge teilen. Meine Nachbarin nebenan hat Ihren Router an der selben Wand stehen, an der auch meiner steht aber auch das Haus auf der anderen Strassenseite hat einige Freifunk Router.

Wir haben nicht alle den selben Anschluss, nicht die gleiche Leistung gebucht. Aber wenn bei einem das Internet ausfällt, kann sein Router einfach über meinen Internetanschluss arbeiten - er kann weiter arbeiten, Netflixgucken oder was auch immer. Wie einigen sich die Router darüber, wer im Zweifelsfall seinen Internetzugang spendet?

== In diesem Artikel
Gehen wir darauf ein, wie Meshrouter sich gegenseitig erkennen und jeweils die optimalen Routen zueinander mittels des B.A.T.M.A.N Protokolls finden. Im besonderen ziehen wir zum Ende hin einen Vergleich der zwei aktuellen Protokoll Versionen B.A.T.M.A.N IV und B.A.T.M.A.N V.

== Die Grundlage
Bildet in jedem Fall ein existierendes Mesh Netzwerk. Was ist das? Es gibt verschiedene Ansätze, wie kabellose Verbindungen zwischen Routern zustande kommen: Zum Beispiel nach dem Standard IEEE 802.11s.

Ein solches Netzwerk wird zum Mesh, wenn es unter anderem diese Eigenschaften erfüllt: Die Knoten (Router) im Netzwerk verbinden sich direkt untereinander und sind jederzeit flexibel darin, Verbindungen zu verlieren oder neu einzurichten. Somit sind die einzelnen Knoten unabhängig und können, solange mindestens zwei Knoten eine Verbindung zueinander haben, immer mit einander kommunizieren.

=== Hallo Nachbar
Sobald ein solches Netzwerk aufgebaut ist, weiss jeder verbundene Knoten, mit welchen unmittelbaren Nachbarn er es zu tun hat. Mit diesem eingeschränkten Horizont ist es nun notwendig das die Knoten miteinander Nachrichten austauschen können die über Ihren Horizont hinaus gehen, hierzu organisieren sich die Router jeweils eine IP Adresse.
Da es in einem Mesh Netzwerk aber keine Hierarchie gibt müssen entweder von Hand die IP (v4) Adressen verteilt werden oder jeder Router berechnet seine einzigartige IP (v6) Adresse auf Basis seiner MAC Adresse (eine Art Seriennummer, die jedes Netzwerkfähige Gerät besitzt). Damit wir nicht von Hand jede IP Adresse vergeben möchten, verlassen wir uns erst einmal auf die errechneten IPv6 Adressen.

=== über den Tellerrand #cite("BATMAN_IV", "BATMAN_NETWORK")
Somit haben unsere Router jetzt die Möglichkeit, sich gegenseitig Nachrichten per auf Basis des Internet Protokolls zu schicken - nun beginnt die Phase des kennenlernens. Alle Router beginnen damit, UDP Nachrichten als Broadcast zu versenden. Das heisst, die Nachrichten werden abgeschickt ohne einen bestimmten Empfänger zu kennen und jeder der diese Nachricht erhält, wird Sie lesen. Die Nachricht enthält unter anderem die Information, von welchem Router (von welcher IP Adresse) sie versendet wurde und die wievielte Nachricht es ist, die dieser bereits verschickt hat.

Jeder Router der eine solche OGM (Originator Message - Ursprungsmeldung) erhält, merkt sich ausserdem die von welchem Nachbarn (von welcher Hardware Adresse) diese Nachricht kam; die Anzahl der erhaltenen Nachrichten merkt er sich für jeden direkten Nachbarn. Als nächstes wird der Router die selbe Nachricht wieder abschicken - an alle von Ihm erreichbaren Knoten.

=== Qualitätsmaße #cite("BATMAN_IV")
Unter anderem erhält nun der erste Knoten seine Nachricht zurück. Er sieht wiederum von wem die Nachricht zurückkam und zählt mit. Nachdem er nun einige seiner eigenen Nachrichten von dem selben Nachbarn zurückerhalten hat, kann er nun ausserdem feststellen wie zuverlässig die Verbindung zu seinem Nachbarn ist.
Denn: Er weiss selbst, wieviele Nachrichten er bisher versendet hat und nun weiss er auch, welche dieser Nachrichten von dem Nachbarn zurückkamen.

Er hält fest dass er bisher eine Zahl X an Nachrichten von seinem Nachbarn bekommen hat (die Empfangsqualität - wird für einen festen Zeitraum als empfangene Pakete/Zeit bestimmt) und von diesen Y Pakete ursprünglich von Ihm selbst kamen - also die Anzahl der Echos aus einer Richtung/von einem Nachbarn; Die Echozahl ist also durch die Verbindungsprobleme von zwei Übertragungen beeinträchtigt.

Für die Kommunikation im Mesh interessiert sich jeder Knoten vor allem dafür, wie gut seine Nachrichten jeweils bei den Nachbarn ankommen - er schätzt also die Sendequalität zu allen Nachbarn ab, mit der Formel TQ = [Echozahl pro Zeiteinheit/ RQ (Empfangsqualität)]. Somit bestimmt jeder Knoten zunächst, welche Knoten er erreichen kann und wie gut/mit welcher Wahrscheinlichkeit diese auch seine Nachrichten erhalten.

Auch indirekte Nachbarn erhalten die OGMs des ersten Routers [des Originators], da jeder Knoten OGMs weiterleitet. Damit jeder Knoten die tatsächliche Verbindungsqualität zum Originator abschätzen kann, muss die weitergeleitete OGM also zusätzliche Informationen über die Verbindungsqualität zwischen Relay-Knoten und dem Originator enthalten.
Wenn der Originator also die OGM absetzt, liegt die Qualität bei 100%. Jeder Nachbar passt den Wert dann auf Grundlage seiner Sendequalität zum vorhergehenden Knoten [TQ] an: Er multipliziert die TQ mit dem Qualitätswert der OGM. Das Ergebnis definiert den neuen Qualitätswert der OGM, die er jetzt weiterleitet.

Nachdem einige OGMs durch das Netz geflogen sind, hat nun jeder Knoten eine Tabelle mit allen direkten Nachbarn sowie eine weitere Tabelle mit allen Knoten, die er nicht direkt sieht sowie die jeweilige Sendequalität zu allen.

Da alle Knoten untereinander Ihre Hardware Adressen kennen, kann das Mesh Netzwerk jetzt wie ein grosser Switch auf Layer 2 betrachtet werden. Wurden bisher alle Nachrichten solange von allen Teilnehmern wiederholt (was eine Menge Traffik, Auslastung verursacht), so kann ein Knoten jetzt abschätzen auf welchem Weg er jeden anderen Router am besten erreichen kann - denn er weiss, über welche Nachbarn er welche Sendequalität erreicht.

Somit kann jeder Knoten eine Route zu jedem Teilnehmer planen und Nachrichten (beliebiger Art: TCP, UDP, QUIC..) aufgeben - auf dem Weg wird meist die beste Qualität erreicht und möglichst wenig Knoten werden auf dem Weg passiert.

Bisher bewegen wir uns ausschliesslich im Bereich eines (W)LAN, eines (Wireless) Local Area Networks - also ohne eine Verbindung ins Internet.

=== Das Internet #cite("BATMAN_NETWORK")
Sobald jetzt aber ein Knoten Zugang zum Internet erhält, wandelt sich seine Rolle im Netzwerk: Er verkündet nun mit jeder OGM, das er eine Verbindung zum Internet hat und Nachrichten/Pakete, die nicht in das LAN gehören über Ihn ausgeleitet werden können. Er wird zum _Gateway_. Wenn er nicht der einzige Gateway Knoten ist, so können die anderen _Client_ Knoten nun nach Ihrer Sendequalität das für sie beste Gateway wählen und selber ins Internet gehen.

=== Der neue #cite("BATMAN_V")
Das bis hier beschrieben Verfahren (es handelt sich um B.A.T.M.A.N IV) funktioniert soweit verlässlich, die meisten Freifunkrouter sind wahrscheinlich tatsächlich Gateways, denn nur wenige Menschen betreiben gleich mehrere Router oder besitzen keinen funktionierenden Internetanschluss.

In einem Szenario, in dem nun aber starke Geschwindigkeits Unterschiede (zum Internet) bei den Gateways bestehen und eventuell auch mehr Client Knoten existieren, wird es schnell interessant beim der Wahl des Gateways nicht nur die Sendeleistung zu berücksichtigen sondern viel mehr auch die Geschwindigkeit miteinzubeziehen.

Dieser Ruf wurde erhört und die Entwicklung von B.A.T.M.A.N V begann. Dieses verspricht, neben der Sendequalität die Geschwindigkeiten stärker zu gewichten: Sowohl die Geschwindigkeiten der Gateways als auch (und vor allem) die der Knoten zueinander.

Die Geschwindigkeit der Gateways ist weitgehend bekannt, die Geschwindigkeit der einzelnen Knoten ist allerdings kniffliger.
Noch vor einigen Generationen haben uns die WLAN Chips der Router fröhlich Auskunft gegeben, mit welchen Datenraten sie aktuell arbeiten doch diese Offenheit wird leider immer seltener. Einige Chips geben noch Auskunft über allgemein mögliche Geschwindigkeiten aber das war es auch schon.
Somit müssen wir eigene Tests bemühen, die wieder Bandbreite fressen und sich bisher nicht als besonders verlässlich erwiesen haben. 

B.A.T.M.A.N V ist mitlerweile veröffentlicht, allerdings ist kurz darauf an einigen Stellen aufgefallen, das die Routingentscheidungen noch einiges zu wünschen übrig lassen: Es werden Routen gewählt die einfach nicht sinnvoll sind, zum Beispiel sendet ein Gateway allen Traffic über zwei Nachbarknoten, weil die Gemeldete Geschwindigkeit des zwei Knoten entfernten Gateways etwas höher ist.

=== Schöne Wolke haben wir hier..?
Nachdem ähnliche Erfahrungen auch von anderen Nutzern gemeldet wurden, ist B.A.T.M.A.N V wieder in der Erprobungsschiene und wir sind zurückgerollt, zum altbekannten IV.

Wie funktioniert das jetzt weiter, wir haben eine Wolke(/Mesh) von Routern, die sich alle kennen und in Internet können. Davon haben wir als Nutzer aber genau nichts!

Der Clou ist dass jeder Router nicht nur die Verbindung zu seinen Nachbarn aufrecht erhält, nein er kann gleichzeitig eines oder mehere WLANs betreiben - das heisst ich kann mein Handy an jedem dieser Router anmelden. Dieser gibt mir eine IP Adresse (spätestens an den Gateways muss man dann ja doch anfangen, öffentliche IPv6 oder genattete IPv4 Adressen zu verteilen) und sorgt dafür, das meine Daten ins Internet gelangen.

Die Information, mit welchem Router mein Handy aktuell verbunden ist, wird in einer verteilten Tabelle gespeichert. Das bedeutet für mich, sobald ich aus der Reichweite des Routers gehe aber zeitgleich ein weiterer Router in meiner Nähe ist der ebenfalls zur Wolke gehört, so können die Router dies erkennen und meinen Übergang nahtlos umsetzen - ich bekomme im Idealfall keine Unterbrechung mit.

=== Na na na na na na na na na na na na na na na na... B.A.T.M.A.N

#bibliography("bib.yml")